//Chris Hinson
//Chapter 4 workbench 17

import java.io.*;

public class main {
    public static void main(String args[]) throws IOException
    {
        File file = new File("NumberList.txt");

        PrintWriter  numberList = new PrintWriter(file);

        for (int i=1;i<=100;i++)
        {
            numberList.println(i);
        }

        numberList.close();
    }
}
